.PHONY: clean train-nlu train-core run-core run-server

TEST_PATH=./

help:
	@echo "    clean"
	@echo "        Remove python artifacts and build artifacts."
	@echo "    train-nlu"
	@echo "        Trains a new nlu model using the projects Rasa NLU config"
	@echo "    train-core"
	@echo "        Trains a new dialogue model using the story training data"
	@echo "    run-core"
	@echo "       This will load the assistant in your terminal for you to chat."
	@echo "    run-core-debug"
	@echo "       This will load the assistant in your terminal for you to chat and open debug mode."

clean:
	rm -rf ./models/current

train-nlu:
	python -m rasa_nlu.train -c nlu_config.yml -d ./data/nlu --fixed_model_name nlu -o models --project current

train-core:
	python -m rasa_core.train -s data/stories -d domain.yml -o models/current/dialogue -c config/policies.yml

run-core:
	python -m rasa_core.run --nlu models/current/nlu --core models/current/dialogue

run-core-debug:
	python -m rasa_core.run --nlu models/current/nlu --core models/current/dialogue --debug
