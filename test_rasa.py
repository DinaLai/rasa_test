#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import unittest
import re
import logging
import sys
import importlib
import time
import yaml

from pathlib import Path

#from bot import CustomPolicy
from rasa_core.agent import Agent
from rasa_core.interpreter import RasaNLUInterpreter
from rasa_core import utils
from rasa_core.policies.keras_policy import KerasPolicy
from rasa_core.events import AllSlotsReset
from rasa_core.events import Restarted

import traceback

logger = logging.getLogger(__name__)
debug = False

if sys.version_info[0] == 2:
    reload(sys)
    sys.setdefaultencoding('utf-8')
elif sys.version_info[0] == 3:
    importlib.reload(sys)

def printInfo(message):
    utils.print_color(message, utils.bcolors.OKBLUE)

def printSuccess(message):
    utils.print_color(message, utils.bcolors.OKGREEN)

def printError(message):
    utils.print_color(message, utils.bcolors.FAIL)

def printLine():
    print("--------------------------")


def testFile(agent, filePath):
    tester = TestUtterance()

    with open(filePath) as f:
        testSuits = json.load(f)

    index = 0
    while(index < len(testSuits)):
        printLine()
        printInfo("# testing line " + str(index+1))
        userMessage = testSuits[index]
        printInfo("   user says: " + userMessage)
        index += 1

        expecting = testSuits[index]
        response = agent.handle_message(userMessage)

        tr = None
        if isinstance(expecting, list):
            tr = testMultiResponse(tester, expecting, response)
        else:
            if len(response) > 0:
                tr = testResponse(tester, expecting, response[0])

        if tr == True or tr is None:
            index += 1
        else:
            printError("# test failed at dialogue " + str(index + 1))
            errorInfo = {}
            if isinstance(tr, dict):
                if len(response) > 0:
                    errorInfo["response"] = response[tr["errorIndex"]]
                else:
                    errorInfo["response"] ="None"

                errorInfo["expecting"] = expecting[tr["errorIndex"]]
                errorInfo["dialogue"] = str(index + 1) + " - " + str(tr["errorIndex"] + 1)
            else:
                if len(response) > 0:
                    errorInfo["response"] = response[0]
                else:
                    errorInfo["response"] ="None"

                errorInfo["expecting"] = expecting
                errorInfo["dialogue"] = index + 1

            errorInfo["filePath"] = filePath
            return errorInfo

    printLine()
    printSuccess("test all passed at [" + filePath + "]")
    return None

def testResponse(tester, expecting, response):
    try:
        if expecting == "*":
            printInfo("   expect res: * any word *" )
        else:
            printInfo("   expect res: " + expecting)

        #print(response)
        response_message = ""

        if "text" in response:
            response_message = response["text"]
        elif "image" in response:
            response_message = response["image"]

        printInfo("   real res:   " + response_message)

        printLine()
        # use * for wildcard to bypass test - always True
        if expecting == "*":
            return True
        else:
            tester.testContains(response_message, expecting)
        return True
    except Exception as e:
        print("Exception ", e)
        traceback.print_stack()
        return False

def testMultiResponse(tester, expecting, response):
    index = 0
    while (index < len(expecting)):
        res = ""
        if len(response) == 0:
            res = ""
        else:
            res =response[index]

        if testResponse(tester, expecting[index], res) == False:
            return {
                "errorIndex": index
            }
        index += 1

    return None

class TestUtterance(unittest.TestCase):
    def testContains(self, text1, text2):
        self.assertIn(text2, text1)

def testDir(agent, dirPath="test"):
    # TODO
    print("dirPath: ", dirPath)
    pathlist = Path(dirPath).glob('**/*.json')
    filePaths = []
    for path in pathlist:
        filePaths.append(str(path))

    results = []
    for filePath in filePaths:
        r = testFile(agent, filePath)
        # # tracker = agent.tracker_store.retrieve("default")
        # tracker = agent.tracker_store.get_or_create_tracker("default")
        # printSuccess("# clearing traker with keys: " + json.dumps(list(agent.tracker_store.keys())))
        # print("# tracker befor Restarted: ", tracker.current_slot_values())
        # tracker.update(AllSlotsReset())
        # tracker.update(Restarted())
        # # agent.tracker_store.save(tracker)
        # print("# tracker after Restarted: ", tracker.current_slot_values())

        # TODO: above: reuse the agent, tracker
        agent = Agent.load("./models/current/dialogue", interpreter=interpreter)
        if r is not None:
            results.append(r)

    printLine()
    printSuccess("\n# All test finished\n")
    printLine()

    print("These files have been tested: ", filePaths)
    if len(results) > 0:

        printInfo("Error test cases as following:")
        printLine()

        for err in results:
            printInfo("Error test case: " + err["filePath"])
            printError("   failed at dialogue " + str(err["dialogue"]))
            printInfo("   expecting: " + str(err["expecting"]))
            printInfo("   real response:   " + str(err["response"]))
            # printError("   the failed response is: " + json.dumps(err["response"]))
            printLine()

    printInfo("Total tested count: " + str(len(filePaths)))
    printInfo("Total failed: " + str(len(results)))

    return


interpreter = RasaNLUInterpreter("./models/current/nlu")

if __name__ == '__main__':
    # utils.configure_colored_logging(loglevel="INFO")

    start_time = time.time()

    agent = Agent.load("./models/current/dialogue", interpreter=interpreter)

    if sys.argv[1] == "-r":
        testDir(agent, sys.argv[2])
    else:
        print( sys.argv[1] );
        r = testFile(agent, sys.argv[1])
        print("Result:%s" % json.dumps(r, indent=2, ensure_ascii=False))

    print("--- %s seconds ---" % (time.time() - start_time))
    exit(1)
