## intent:greet
- 你好
- 你好啊
- 你好嗎
- hello
- hi
- 早上好
- 晚上好
- 嗨
- Hi
- 嗨
- 你好，聊天機器人
- 哈囉
- 早安
- 午餐
- 晚安
- 你好，有人在嗎?
- hi
- hello
- hey
- HI

## intent:goodbye
- bye
- 再見
- 886
- 拜拜
- 下次見
- 掰掰
- 晚點見
- 再見，我的朋友
- 明天見
- 改天見
- 祝你有美好的一天
- 我要離開了
- 我要下線了
- 期待下次見面
- bye
- goodbye

## intent:thanks
- 謝謝你
- 謝謝
- thanks
- 感謝
- Thank you
- 感謝你的幫忙
- 謝謝你
- 謝謝妳
- 謝謝您
- 感謝你的幫忙
- 感謝你的幫助
- 感謝你的協助
- 感謝妳的幫忙
- 感謝妳的幫助
- 感謝妳的協助
- 好的，謝謝
- 非常感謝
- 非常感謝您
- 非常謝謝你
- Thanks
- Thank you
- thx
- 3q
